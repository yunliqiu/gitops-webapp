FROM scratch

LABEL maintaner="Yunli Qiu <yunli.qiu@gmail.com>"

COPY . .

EXPOSE 8080

CMD ["./main"]
